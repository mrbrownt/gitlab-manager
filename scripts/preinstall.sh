#!/bin/bash

set -eo pipefail

if [ -f /.dockerenv ] && command -v sudo; then
  sudo chown -R "$USER:$USER" /workspaces
fi
