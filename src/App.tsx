import React from "react"
import {
  createMuiTheme,
  CssBaseline,
  ThemeProvider,
  useMediaQuery,
} from "@material-ui/core"
import Router from "./components/Router"

function App(): JSX.Element {
  const prefersDarkMode = useMediaQuery("(prefers-color-scheme: dark)")

  const theme = createMuiTheme({
    palette: {
      type: prefersDarkMode ? "dark" : "light",
    },
  })

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline>
        <Router />
      </CssBaseline>
    </ThemeProvider>
  )
}

export default App
