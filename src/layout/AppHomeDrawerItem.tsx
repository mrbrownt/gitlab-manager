import {
  Button,
  createStyles,
  List,
  ListItem,
  ListItemIcon,
  makeStyles,
} from "@material-ui/core"
import React from "react"
import { useHistory, useLocation } from "react-router-dom"

const useStyles = makeStyles((theme) =>
  createStyles({
    button: {
      padding: "10px 8px",
      justifyContent: "flex-start",
      textTransform: "none",
      letterSpacing: 0,
      width: "100%",
    },
    item: {
      display: "flex",
      paddingTop: 0,
      paddingBottom: 0,
    },
  })
)

interface Props {
  pages: Page[]
}

export interface Page {
  title: string
  href: string
  icon: React.ReactNode
}

export default function AppHomeDrawerItem({ pages }: Props): JSX.Element {
  const classes = useStyles()
  const history = useHistory()
  const selectedIndex = useSelectedIndex(pages)

  return (
    <List>
      {pages.map((page, index) => (
        <ListItem
          // eslint-disable-next-line react/no-array-index-key
          key={index}
          className={classes.item}
          selected={selectedIndex === index}
        >
          <Button
            className={classes.button}
            onClick={() => {
              history.push(page.href)
            }}
          >
            <ListItemIcon>{page.icon}</ListItemIcon>
            {page.title}
          </Button>
        </ListItem>
      ))}
    </List>
  )
}

function useSelectedIndex(pages: Page[]): number {
  const location = useLocation()
  const [selectedIndex, setSelectedIndex] = React.useState(10000)

  React.useEffect(() => {
    pages.forEach((page, index) => {
      if (location.pathname.startsWith(page.href)) {
        setSelectedIndex(index)
      }
    })
  })

  return selectedIndex
}
