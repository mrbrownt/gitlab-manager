import { Container, createStyles, makeStyles } from "@material-ui/core"
import React from "react"
import AppHomeDrawer from "./AppHomeDrawer"

const useStyles = makeStyles((theme) =>
  createStyles({
    shiftContent: { paddingLeft: 240 },
  })
)

interface Props {
  children: React.ReactNode
}

export default function AppHomeLayout({ children }: Props): JSX.Element {
  const classes = useStyles()
  return (
    <Container className={classes.shiftContent}>
      <AppHomeDrawer />
      {children}
    </Container>
  )
}
