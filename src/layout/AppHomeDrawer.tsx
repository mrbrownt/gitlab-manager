import {
  Button,
  createStyles,
  Drawer,
  List,
  ListItem,
  makeStyles,
} from "@material-ui/core"
import { Home, Person } from "@material-ui/icons"
import React from "react"
import AppHomeDrawerItem, { Page } from "./AppHomeDrawerItem"

const useStyles = makeStyles((theme) =>
  createStyles({
    drawer: { width: 240 },
  })
)

export default function AppHomeDrawer(): JSX.Element {
  const classes = useStyles()
  return (
    <Drawer
      anchor="left"
      variant="permanent"
      classes={{ paper: classes.drawer }}
    >
      <AppHomeDrawerItem pages={pages} />
    </Drawer>
  )
}

const pages: Page[] = [
  {
    title: "All Projects",
    href: "/app/all_projects",
    icon: <Home />,
  },
  {
    title: "My Merge Requets",
    href: "/app/my_merge_requests",
    icon: <Person />,
  },
]
