export function useGitlabRedirectURL(): string {
  const urlBase = process.env.PUBLIC_URL || window.location.toString()
  return encodeURIComponent(`${urlBase}auth/callback`)
}

export function useGitlabAppID(): string {
  return process.env.REACT_APP_GITLAB_APP_ID as string
}

export function useGitlabAppSecret(): string {
  return process.env.REACT_APP_GITLAB_APP_SECRET as string
}
