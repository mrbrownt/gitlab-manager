import * as crypto from "crypto"

export function useAuthState(): string {
  let authState = localStorage.getItem("gitlabAuthState")
  if (authState === null) {
    authState = crypto.randomBytes(20).toString("hex")
    localStorage.setItem("gitlabAuthState", authState)
  }

  return authState
}
