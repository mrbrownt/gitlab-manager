import Axios from "axios"

const api = Axios.create({ timeout: 5000 })

export async function getGitlabToken(): Promise<string> {
  const response = api.get<string>("/api/auth/gitlab/token")
  return (await response).data
}
