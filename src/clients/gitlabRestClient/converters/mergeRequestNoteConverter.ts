// To parse this data:
//
//   import { Convert } from "./file";
//
//   const mergeRequestNote = Convert.toMergeRequestNote(json);

export interface MergeRequestNote {
  id:               number;
  type?:            null;
  body:             string;
  attachment:       null;
  author:           Author;
  createdAt:        Date;
  updatedAt:        Date;
  system:           boolean;
  noteableID:       number;
  noteableType:     NoteableType;
  resolvable:       boolean;
  confidential:     boolean;
  noteableIid:      number;
  commandsChanges?: CommandsChanges;
}

export interface Author {
  id:         number;
  name:       string;
  username:   string;
  state:      State;
  avatarURL?: string;
  webURL?:    string;
  email?:     string;
  createdAt?: Date;
}

export enum State {
  Active = "active",
}

export interface CommandsChanges {
}

export enum NoteableType {
  Issue = "Issue",
  MergeRequest = "MergeRequest",
}

// Converts JSON strings to/from your types
export class Convert {
  public static toMergeRequestNote(json: string): MergeRequestNote {
      return JSON.parse(json);
  }

  public static mergeRequestNoteToJson(value: MergeRequestNote): string {
      return JSON.stringify(value);
  }

  public static toAuthor(json: string): Author {
      return JSON.parse(json);
  }

  public static authorToJson(value: Author): string {
      return JSON.stringify(value);
  }

  public static toCommandsChanges(json: string): CommandsChanges {
      return JSON.parse(json);
  }

  public static commandsChangesToJson(value: CommandsChanges): string {
      return JSON.stringify(value);
  }
}
