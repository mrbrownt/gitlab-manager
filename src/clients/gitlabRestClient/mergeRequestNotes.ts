import { gitlabClient } from "."
import {
  Convert,
  MergeRequestNote,
} from "./converters/mergeRequestNoteConverter"

interface getMergeRequestNotesParams {
  projectID: string
  mergeRequestIID: string | number
  sort?: "asc" | "desc"
  orderBy?: "created_at" | "updated_at"
}

// eslint-disable-next-line import/prefer-default-export
export async function getMergeRequestNotes({
  projectID,
  mergeRequestIID,
  sort = "desc",
  orderBy = "created_at",
}: getMergeRequestNotesParams): Promise<MergeRequestNote[]> {
  const api = gitlabClient()
  const encodedProject = projectID.replaceAll("/", "%2F")
  const path = `/projects/${encodedProject}/merge_requests/${mergeRequestIID}/notes?sort=${sort}&order_by=${orderBy}`

  const response = await api.get(path)

  return response.data.map((note: unknown) =>
    Convert.toMergeRequestNote(JSON.stringify(note))
  )
}
