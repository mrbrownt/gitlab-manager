import Axios, { AxiosInstance } from "axios"

interface gitlabClientConfig {
  baseURL?: string
  oauthToken: string
}

let api: AxiosInstance

export function setupRestClient({
  baseURL = "https://gitlab.com",
  oauthToken,
}: gitlabClientConfig): void {
  api = Axios.create({
    baseURL: `${baseURL}/api/v4`,
    headers: {
      authorization: `Bearer ${oauthToken}`,
    },
  })
}

export function gitlabClient(): AxiosInstance {
  return api
}
