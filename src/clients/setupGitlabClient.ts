import { persistCache, PersistentStorage } from "apollo3-cache-persist"
import {
  InMemoryCache,
  ApolloClient,
  ApolloLink,
  HttpLink,
  NormalizedCacheObject,
} from "@apollo/client/core"

function authMiddleware(token?: string) {
  return new ApolloLink((operation, forward) => {
    if (token) {
      operation.setContext({
        headers: {
          authorization: `Bearer ${token}`,
        },
      })
    }

    return forward(operation)
  })
}

const cache = new InMemoryCache()
const host = new HttpLink({ uri: "https://gitlab.com/api/graphql" })

export default async function setupGraphqlClient(
  token?: string
): Promise<ApolloClient<NormalizedCacheObject>> {
  await persistCache({
    cache,
    storage: window.localStorage,
  })

  return new ApolloClient({
    link: authMiddleware(token).concat(host),
    cache,
  })
}
