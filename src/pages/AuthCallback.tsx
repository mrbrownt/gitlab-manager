import { Typography } from "@material-ui/core"
import Axios from "axios"
import React from "react"
import { useHistory, useLocation } from "react-router-dom"
import { useToken } from "../components/contexts/TokenContext"
import {
  useGitlabAppID,
  useGitlabAppSecret,
  useGitlabRedirectURL,
} from "../hooks/gitlabHooks"
import { useAuthState } from "../hooks/useAuthState"

function useQuery() {
  return new URLSearchParams(useLocation().search)
}

export default function AuthCallback(): JSX.Element {
  const query = useQuery()
  const code = query.get("code")
  const state = query.get("state")
  const appID = useGitlabAppID()
  const appSecret = useGitlabAppSecret()
  const knownState = useAuthState()
  const redirectURL = useGitlabRedirectURL()
  const history = useHistory()
  const { token, setToken } = useToken()

  React.useEffect(() => {
    async function fetchToken(): Promise<string> {
      const parameters = `?client_id=${appID}&client_secret=${appSecret}&code=${code}&grant_type=authorization_code&redirect_uri=${redirectURL}`
      const response = await Axios.post<TokenResponse>(
        `/oauth/token${parameters}`,
        undefined,
        {
          baseURL:
            process.env.NODE_ENV === "development"
              ? `http://${window.location.host}`
              : "https://gitlab.com",
        }
      )

      return response.data.access_token
    }

    fetchToken()
      .then((newToken) => {
        setToken(newToken)
      })
      .catch((error) => {
        // eslint-disable-next-line no-console
        console.error(error)
      })
  }, [appID, appSecret, code, redirectURL, setToken])

  React.useEffect(() => {
    if (token !== null) {
      history.push("/app/all_projects")
    }
  }, [history, token])

  if (state !== knownState) {
    return <Typography variant="h1">Something has been hijacked</Typography>
  }

  return <div />
}

/* eslint-disable camelcase */
interface TokenResponse {
  access_token: string
  token_type: "bearer"
  expires_in: number
  refresh_token: string
}
/* eslint-enable camelcase */
