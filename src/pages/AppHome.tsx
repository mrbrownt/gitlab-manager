import {
  ApolloClient,
  ApolloProvider,
  NormalizedCacheObject,
  QueryResult,
} from "@apollo/client"
import React from "react"
import { setupRestClient } from "../clients/gitlabRestClient"
import setupGraphqlClient from "../clients/setupGitlabClient"
import { useToken } from "../components/contexts/TokenContext"
import MyMergeRequests from "./MyMergeRequests/MyMergeRequests"

export default function AppHome(): JSX.Element {
  const { token } = useToken()
  const [graphqlClient, setGraphqlClient] = React.useState<
    ApolloClient<NormalizedCacheObject>
  >()

  React.useEffect(() => {
    async function configureGraphqlClient(gitlabToken: string) {
      setGraphqlClient(await setupGraphqlClient(gitlabToken))
    }

    if (token) {
      configureGraphqlClient(token)
      setupRestClient({ oauthToken: token })
    }
  }, [token])

  // TODO: Proper loading component
  if (!graphqlClient) {
    return <div>Loading</div>
  }

  return (
    <ApolloProvider client={graphqlClient}>
      <MyMergeRequests />
    </ApolloProvider>
  )
}
