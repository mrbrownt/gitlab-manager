import {
  Button,
  Checkbox,
  createStyles,
  makeStyles,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Theme,
  Toolbar,
  Typography,
} from "@material-ui/core"
import React from "react"
import { useActiveMergeRequestsQuery } from "../../generated/graphql"
import Row from "../../components/MergeRequestRow"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    title: {
      flex: "1 1 25%",
    },
  })
)

export default function ReviewMergeRequests(): JSX.Element {
  const classes = useStyles()
  const { data, loading } = useActiveMergeRequestsQuery({
    pollInterval: 60_000,
  })
  const [mergeRequests, setMergeRequests] = React.useState(
    data?.currentUser?.reviewRequestedMergeRequests?.nodes
  )
  const [selected, setSelected] = React.useState<string[]>([])

  React.useEffect(() => {
    if (!loading) {
      setMergeRequests(data?.currentUser?.reviewRequestedMergeRequests?.nodes)
    }
  }, [data?.currentUser?.reviewRequestedMergeRequests?.nodes, loading])

  if (loading) {
    return <Typography>Loading</Typography>
  }

  function handleSelectAll(event: React.ChangeEvent<HTMLInputElement>) {
    if (event.target.checked) {
      const newSelected = mergeRequests?.map(
        (mergeRequest) => mergeRequest?.id ?? ""
      )
      setSelected(newSelected ?? [])
      return
    }
    setSelected([])
  }

  function handleSelection(id?: string) {
    if (!id) {
      return
    }

    const selectedIndex = selected.indexOf(id)
    let newSelected: string[] = []
    switch (selectedIndex) {
      case -1:
        newSelected = newSelected.concat(selected, id)
        break
      case 0:
        newSelected = newSelected.concat(selected.slice(1))
        break
      case selected.length - 1:
        newSelected = newSelected.concat(selected.slice(0, -1))
        break
      default:
        newSelected = newSelected.concat(
          selected.slice(0, selectedIndex),
          selected.slice(selectedIndex + 1)
        )
        break
    }

    setSelected(newSelected)
  }

  function noRowsSelected(): boolean {
    return selected.length === 0
  }

  return (
    <>
      <Toolbar>
        <Typography className={classes.title} variant="h6" id="tableTitle">
          Review Requests
        </Typography>
        <Button disabled={noRowsSelected()}>Merge</Button>
        <Button disabled={noRowsSelected()}>Approve</Button>
        <Button disabled={noRowsSelected()}>Approve and Merge</Button>
      </Toolbar>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell padding="checkbox">
              <Checkbox onChange={handleSelectAll} />
            </TableCell>
            <TableCell>Title</TableCell>
            <TableCell>Pipeline</TableCell>
            <TableCell>Approval</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {mergeRequests?.map((mergeRequest) => (
            <Row
              key={mergeRequest?.id}
              mergeRequest={mergeRequest}
              handleSelection={handleSelection}
              selection={selected}
            />
          ))}
        </TableBody>
      </Table>
    </>
  )
}
