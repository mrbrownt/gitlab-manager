/* eslint-disable no-await-in-loop */
/* eslint-disable no-restricted-syntax */
import {
  Button,
  Checkbox,
  createStyles,
  makeStyles,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableFooter,
  TableHead,
  TableRow,
  Theme,
  Toolbar,
  Typography,
} from "@material-ui/core"
import React from "react"
import {
  useAcceptMergeRequestMutation,
  useActiveMergeRequestsQuery,
} from "../../generated/graphql"
import MergeRequestRow from "../../components/MergeRequestRow"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    title: {
      flex: "1 1 25%",
    },
  })
)

export default function AssignedMergeRequests(): JSX.Element {
  // Style
  const classes = useStyles()

  // Graphql
  const { data, loading, refetch } = useActiveMergeRequestsQuery({
    pollInterval: 60_000,
  })
  const [
    acceptMergeRequestMutation,
    { data: updateMergeRequestData },
  ] = useAcceptMergeRequestMutation()

  const [mergeRequests, setMergeRequests] = React.useState(
    data?.currentUser?.assignedMergeRequests?.nodes
  )
  const [selected, setSelected] = React.useState<string[]>([])

  // Local state
  React.useEffect(() => {
    if (!loading) {
      setMergeRequests(data?.currentUser?.assignedMergeRequests?.nodes)
    }
  }, [data?.currentUser?.assignedMergeRequests?.nodes, loading])

  const noRowsSelected = React.useMemo(() => selected.length === 0, [
    selected.length,
  ])

  if (loading) {
    return <Typography>Loading</Typography>
  }

  function handleSelectAll(event: React.ChangeEvent<HTMLInputElement>) {
    if (event.target.checked) {
      const newSelected = mergeRequests?.map(
        (mergeRequest) => mergeRequest?.id ?? ""
      )
      setSelected(newSelected ?? [])
      return
    }
    setSelected([])
  }

  function handleSelection(id?: string) {
    if (!id) {
      return
    }

    const selectedIndex = selected.indexOf(id)
    let newSelected: string[] = []
    switch (selectedIndex) {
      case -1:
        newSelected = newSelected.concat(selected, id)
        break
      case 0:
        newSelected = newSelected.concat(selected.slice(1))
        break
      case selected.length - 1:
        newSelected = newSelected.concat(selected.slice(0, -1))
        break
      default:
        newSelected = newSelected.concat(
          selected.slice(0, selectedIndex),
          selected.slice(selectedIndex + 1)
        )
        break
    }

    setSelected(newSelected)
  }

  async function acceptMergeRequests() {
    for (const selectedMergeRequset of selected) {
      const mergeRequestToApprove = mergeRequests?.find(
        (mergeRequest) => mergeRequest?.id === selectedMergeRequset
      )

      if (mergeRequestToApprove) {
        await acceptMergeRequestMutation({
          variables: {
            input: {
              projectPath: mergeRequestToApprove.project.fullPath,
              iid: mergeRequestToApprove.iid,
              sha: mergeRequestToApprove.diffHeadSha ?? "",
            },
          },
        })
      }
    }

    await refetch()
  }

  return (
    <TableContainer component={Paper}>
      <Toolbar>
        <Typography className={classes.title} variant="h6" id="tableTitle">
          Assgined Merge Requests
        </Typography>
        <Button disabled={noRowsSelected} onClick={acceptMergeRequests}>
          Merge
        </Button>
        <Button disabled={noRowsSelected}>Approve</Button>
        <Button disabled={noRowsSelected}>Approve and Merge</Button>
      </Toolbar>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell padding="checkbox">
              <Checkbox onChange={handleSelectAll} />
            </TableCell>
            <TableCell>Title</TableCell>
            <TableCell>Pipeline</TableCell>
            <TableCell>Approval</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {mergeRequests?.map((mergeRequest) => (
            <MergeRequestRow
              key={mergeRequest?.id}
              mergeRequest={mergeRequest}
              handleSelection={handleSelection}
              selection={selected}
            />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  )
}
