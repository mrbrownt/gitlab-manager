import { Container, Grid, List, ListItem, Paper } from "@material-ui/core"
import React from "react"
import AssignedMergeRequests from "./AssignedMergeRequests"

import ReviewMergeRequests from "./ReviewMergeRequests"

export default function MyMergeRequests(): JSX.Element {
  return (
    <Container>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Paper>
            <ReviewMergeRequests />
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <AssignedMergeRequests />
        </Grid>
      </Grid>
    </Container>
  )
}
