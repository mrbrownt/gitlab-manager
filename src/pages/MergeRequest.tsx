import React from "react"
import { useParams } from "react-router-dom"
import {
  Card,
  CardContent,
  CardHeader,
  List,
  ListItem,
} from "@material-ui/core"
import parse from "html-react-parser"
import { MergeRequestNote } from "../clients/gitlabRestClient/converters/mergeRequestNoteConverter"
import { getMergeRequestNotes } from "../clients/gitlabRestClient/mergeRequestNotes"

export default function MergeRequest(): JSX.Element {
  const { projectID, mergeRequestIID } = useParams<{
    projectID: string
    mergeRequestIID: string
  }>()
  const [notes, setNotes] = React.useState<MergeRequestNote[]>()

  React.useEffect(() => {
    async function getMR() {
      const response = await getMergeRequestNotes({
        projectID,
        mergeRequestIID,
      })

      setNotes(response)
    }

    getMR()
  }, [mergeRequestIID, projectID])

  return (
    <List>
      {notes?.map((note) => (
        <ListItem key={note.id}>
          <Card>
            <CardHeader title={note.author.name} />
            <CardContent>{parse(note.body)}</CardContent>
          </Card>
        </ListItem>
      ))}
    </List>
  )
}
