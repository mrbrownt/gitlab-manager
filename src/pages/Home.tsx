import { Button, Container, Typography } from "@material-ui/core"
import React from "react"
import { useGitlabAppID, useGitlabRedirectURL } from "../hooks/gitlabHooks"
import { useAuthState } from "../hooks/useAuthState"

export default function Home(): JSX.Element {
  const appID = useGitlabAppID()
  const redirectURL = useGitlabRedirectURL()
  const state = useAuthState()
  const urlParams = `client_id=${appID}&redirect_uri=${redirectURL}&response_type=code&state=${state}&scope=api`

  return (
    <Container>
      <Typography variant="h3">Welcome to my shiz</Typography>
      <Button
        variant="outlined"
        href={`https://gitlab.com/oauth/authorize?${urlParams}`}
      >
        Login
      </Button>
    </Container>
  )
}
