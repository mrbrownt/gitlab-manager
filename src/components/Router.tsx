import React, { Suspense } from "react"
import { BrowserRouter, Route, Switch } from "react-router-dom"
import { TokenProvider } from "./contexts/TokenContext"

const Home = React.lazy(() => import("../pages/Home"))
const AppHome = React.lazy(() => import("../pages/AppHome"))
const AuthCallback = React.lazy(() => import("../pages/AuthCallback"))

export default function Router(): JSX.Element {
  return (
    <BrowserRouter>
      <Suspense fallback={<div>Loading</div>}>
        <TokenProvider>
          <Switch>
            <Route exact path="/">
              <Home />
            </Route>
            <Route path="/app">
              <AppHome />
            </Route>
            <Route path="/auth/callback">
              <AuthCallback />
            </Route>
          </Switch>
        </TokenProvider>
      </Suspense>
    </BrowserRouter>
  )
}
