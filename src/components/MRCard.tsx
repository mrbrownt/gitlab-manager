import {
  Button,
  Card,
  CardContent,
  CardHeader,
  createStyles,
  makeStyles,
} from "@material-ui/core"
import React from "react"
import parse from "html-react-parser"
import { MergeType } from "@material-ui/icons"
import { useHistory } from "react-router-dom"
import {
  MergeRequestFragment,
  Maybe,
  useProjectQuery,
} from "../generated/graphql"

const useStyles = makeStyles(() =>
  createStyles({
    card: {
      flexGrow: 1,
    },
  })
)

interface Props {
  mergeRequest: Maybe<MergeRequestFragment>
}

export default function MRCard({ mergeRequest }: Props): JSX.Element {
  const classes = useStyles()
  const history = useHistory()
  const { data } = useProjectQuery({
    variables: { project: mergeRequest?.project.fullPath ?? "" },
  })
  const projectPath = mergeRequest?.project.fullPath.replaceAll("/", "%2F")

  return (
    <Card className={classes.card}>
      <CardHeader
        avatar={<MergeType />}
        title={mergeRequest?.title}
        subheader={data?.project?.name}
      />
      <CardContent>{parse(mergeRequest?.descriptionHtml ?? "")}</CardContent>
    </Card>
  )
}
