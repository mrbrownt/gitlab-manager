import {
  Card,
  CardContent,
  ListItem,
  ListItemIcon,
  ListItemText,
  Typography,
} from "@material-ui/core"
import { BookmarkBorder } from "@material-ui/icons"
import React from "react"
import { Project, useProjectQuery, Maybe } from "../generated/graphql"

export interface Props {
  project: Maybe<
    { __typename?: "Project" } & Pick<
      Project,
      "name" | "archived" | "id" | "fullPath"
    >
  >
  className?: string
}

export default function ProjectCard({
  project,
  className,
}: Props): JSX.Element {
  const { data, loading } = useProjectQuery({
    variables: { project: project?.fullPath ?? "" },
  })

  if (loading) {
    return (
      <Card className={className}>
        <CardContent>
          <Typography variant="h5">Loading: {project?.name}</Typography>
        </CardContent>
      </Card>
    )
  }

  return (
    <Card>
      <CardContent>
        <Typography variant="h5">{project?.name}</Typography>
        <Typography>Open MRs: {data?.project?.mergeRequests?.count}</Typography>
      </CardContent>
    </Card>
  )
}
