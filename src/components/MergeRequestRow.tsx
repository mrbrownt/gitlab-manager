import {
  Checkbox,
  Link,
  TableCell,
  TableRow,
  Typography,
} from "@material-ui/core"
import { blue, green, grey, red } from "@material-ui/core/colors"
import {
  Adjust,
  CancelOutlined,
  ChatBubbleOutline,
  CheckCircleOutline,
  Person,
  RadioButtonChecked,
} from "@material-ui/icons"
import React from "react"
import ReactMarkdown from "react-markdown"
import gfm from "remark-gfm"
import { getMergeRequestNotes } from "../clients/gitlabRestClient/mergeRequestNotes"
import {
  Maybe,
  MergeRequestFragment,
  PipelineStatusEnum,
} from "../generated/graphql"

interface RowProps {
  mergeRequest: Maybe<MergeRequestFragment>
  handleSelection: (id?: string | undefined) => void
  selection: string[]
}

export default function MergeRequestRow({
  mergeRequest,
  handleSelection,
  selection,
}: RowProps): JSX.Element {
  const selected = selection.find((value) => value === mergeRequest?.id)

  const pipelineLink = `https://gitlab.com${mergeRequest?.headPipeline?.path}`

  return (
    <>
      <TableRow selected={selected !== undefined}>
        <TableCell padding="checkbox">
          <Checkbox
            checked={selected !== undefined}
            onClick={() => {
              handleSelection(mergeRequest?.id)
            }}
          />
        </TableCell>
        <TableCell>
          <Typography variant="body2">
            <Link
              href={mergeRequest?.webUrl ?? ""}
              color="inherit"
              target="_blank"
              rel="noreferrer noopener"
            >
              {mergeRequest?.title}
            </Link>
          </Typography>
          <Typography variant="body2" color="textSecondary">
            {mergeRequest?.project.fullPath}!{mergeRequest?.iid}
          </Typography>
        </TableCell>
        <TableCell>
          <Link href={pipelineLink} target="_blank" rel="noreferrer noopener">
            <PipelineStatusIcon
              pipelineStatus={mergeRequest?.headPipeline?.status}
            />
          </Link>
        </TableCell>
        <TableCell>
          <Typography>
            {mergeRequest?.approved ? (
              <Person style={{ color: green[500] }} />
            ) : (
              <Person />
            )}{" "}
            Left {mergeRequest?.approvalsLeft}
          </Typography>
          <Typography variant="body2" color="textSecondary">
            <ChatBubbleOutline fontSize="small" />{" "}
            {mergeRequest?.userNotesCount}
          </Typography>
        </TableCell>
      </TableRow>
    </>
  )
}

interface PipelineStatusIconProps {
  pipelineStatus?: PipelineStatusEnum
}

function PipelineStatusIcon({ pipelineStatus }: PipelineStatusIconProps) {
  switch (pipelineStatus) {
    case undefined:
      return <Adjust style={{ color: grey[500] }} />
    case PipelineStatusEnum.Running:
      return <RadioButtonChecked style={{ color: blue[500] }} />
    case PipelineStatusEnum.Success:
      return <CheckCircleOutline style={{ color: green[500] }} />
    case PipelineStatusEnum.Manual:
      return <CheckCircleOutline style={{ color: green[500] }} />
    default:
      return <CancelOutlined style={{ color: red[500] }} />
  }
}
