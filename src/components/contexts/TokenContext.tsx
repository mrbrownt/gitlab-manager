import React, { useCallback, useReducer } from "react"

interface State {
  token: string | null
}

const initialState: State = {
  token: null,
}

type Action = { type: "setToken"; value: string }

function reducer(state: State, action: Action) {
  switch (action.type) {
    case "setToken":
      return { ...state, token: action.value }
    default:
      throw new Error(`unknown action: ${String(action)}`)
  }
}

const intitalContext: { state: State; dispatch: React.Dispatch<Action> } = {
  state: initialState,
  dispatch: () => undefined,
}

const TokenContext = React.createContext(intitalContext)

export function TokenProvider({
  children,
}: React.PropsWithChildren<Record<string, unknown>>): JSX.Element {
  const [state, dispatch] = useReducer<React.Reducer<State, Action>>(
    reducer,
    initialState
  )

  return (
    <TokenContext.Provider value={{ state, dispatch }}>
      {children}
    </TokenContext.Provider>
  )
}

interface SetTokenState extends State {
  setToken: (tokenState: string) => void
}

export const useToken = (): SetTokenState => {
  const { state, dispatch } = React.useContext(TokenContext)

  const setToken = useCallback(
    (tokenState: string) => {
      dispatch({ type: "setToken", value: tokenState })
    },
    [dispatch]
  )

  return { ...state, setToken }
}
